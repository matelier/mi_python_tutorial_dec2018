for NAME in *.ipynb */*.ipynb;
do echo $NAME
jupyter nbconvert --to notebook --inplace --Exporter.preprocessors='["nbconvert.preprocessors.clearoutput.ClearOutputPreprocessor"]' $NAME
done 
